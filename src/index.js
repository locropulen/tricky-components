/**
 * @flow
 */

module.exports = {
  // Core
  get StacksInTabs() {
    return require('./StacksInTabs').default;
  },
  get Drawer() {
    return require('./Drawer').default;
  },
  get Banner() {
    return require('./Banner').default;
  },
  get SimpleTabs() {
    return require('./SimpleTabs').default;
  },
  get SimpleStack() {
    return require('./SimpleStack').default;
  },
}
